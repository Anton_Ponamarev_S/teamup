import { mapActions, mapGetters } from 'vuex';

export default {
  data: () => ({
    users: [],
  }),

  created () {
    this.fetchData()
  },
  methods: {
    ...mapActions(['getUsersList', 'getAvatars']),
    ...mapGetters(['getUsersAll', 'getAvatarsUrl']),

    async loadImage() {
      const avatars = new Array(this.users.lenght)
      this.users.forEach((user, index) => {
        avatars[index] = user.avatar
      })

      await this.getAvatars(avatars)
        .then(() => {
          const usersUrl = this.getAvatarsUrl()

          this.users.forEach((user, index) => {
            this.$set(user, 'imageUrl', usersUrl[index])
          })
        })
    },

    async fetchData() {
      await this.getUsersList()
        .then(async () => {
          this.users = this.getUsersAll()
          await this.loadImage()
        })
    }
  }
}
