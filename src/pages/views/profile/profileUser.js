import { mapActions, mapGetters } from 'vuex';
import ButtonTeamUp from './components/ButtonTeamUp'
export default {
  name: 'profile',
  data: () => ({
    user: {
      resume: {}
    }
  }),
  components: {
    ButtonTeamUp
  },
  created () {
    this.fetchData()
  },

  methods: {
    ...mapActions(['getUserProfileUser', 'getAvatars']),
    ...mapGetters(['getProfileUser', 'getAvatarsUrl', 'getUser', 'getIsAuth']),

    userDto(user_data) {
      this.user = user_data
      this.user.resume.skills = this.user.resume?.skills?.split('#')
    },

    async loadImage() {
      const avatars = new Array(1)
      avatars[0] = this.user.avatar

      await this.getAvatars(avatars)
        .then(() => {
          const membersUrl = this.getAvatarsUrl()
          this.$set(this.user, 'imageUrl', membersUrl[0])
        })
    },

    async fetchData() {
      await this.getUserProfileUser(this.$route.params.userId)
      .then(async () => {
        this.userDto(this.getProfileUser())
        await this.loadImage()
      })
    },
  }
}
