import { mapActions, mapGetters, mapMutations } from 'vuex';
import { required } from 'vuelidate/lib/validators'
import Applications from './components/Applications'

export default {
  name: 'profile',
  components: {
    Applications
  },
  data: () => ({
    isEdit: false,
    tab: ['resume', 'applications'],
    currentTab: 'resume',

    avatar: {
      image: '',
    },

    user: {
      name: '',
      contacts: [{
        contact_name: '',
        url: ''
      }],
      resume: {
        skills: []
      },
      // candidates: {
      //   project: {}
      // }
    },

    editUser: {
      contacts: [],
      resume: {}
    }
  }),

  validations: {
    user: {
      name: { required },
      contacts: {
        $each: {
          contact_name: { required },
          url: { required }
        }
      },
      resume: {
        skills: {
          $each: { required }
        }
      }
    }
  },

  created () {
    this.fetchData()
  },

  methods: {
    ...mapActions(['getUserProfile', 'editProfile', 'setAvatar', 'getUserAvatar']),
    ...mapGetters(['getProfile', 'getAvatarUserUrl', 'getUser']),
    ...mapMutations(['setUserAvatarUrl']),

    onChangeName() { this.editUser.name = this.user.name },
    onChangeAge() { this.editUser.age = this.user.age },
    onChangeOrganization() { this.editUser.organization = this.user.organization },
    onChangeProfession() { this.editUser.resume.profession = this.user.resume.profession },
    onChangeAboutMe() { this.editUser.resume.about_me = this.user.resume.about_me },
    onChangeSkills() { this.editUser.resume.skills = this.user.resume.skills },
    onChangeContactUrl(contact) {
      if (contact.isOld) {
        for (let editContact of this.editUser.contacts) {
          if(editContact.contact_name === contact.contact_name) {
            editContact.url = contact.url
            return
          }
        }
        this.editUser.contacts.push({
          contact_name: contact.contact_name,
          url: contact.url
        })
      }
    },

    pushContacts() {
      this.user.contacts.push({
        contact_name: '',
        url: ''
      })
    },

    spliceContacts(index) {
      if (this.user.contacts[index].isOld) {
        this.editUser.contacts.push({
          contact_name: this.user.contacts[index].contact_name,
          url: ''
        })
      }
      this.user.contacts.splice(index, 1)
    },

    pushSkill() {
      this.user.resume.skills.push('')
    },

    spliceSkill(index) {
      this.user.resume.skills.splice(index, 1)
      this.editUser.resume.skills = this.user.resume.skills
    },

    async onChangeImage() {
      this.avatar.image = this.$refs.file.files[0];
      const fileReaderInstance = new FileReader();
      fileReaderInstance.readAsDataURL(this.avatar.image);
      fileReaderInstance.onload = () => {
        this.setUserAvatarUrl(fileReaderInstance.result)
      }
    },

    userDto(user_data) {
      this.user = user_data
      this.user.contacts?.forEach(contact => contact.isOld = true)
      this.user.resume.skills = this.user.resume?.skills?.split('#')
    },

    async fetchData() {

      await this.getUserProfile()
      .then(async () => {
        this.userDto(this.getProfile())
        await this.getUserAvatar(this.user.avatar)
      })

    },

    async editData() {
      if (this.$v.$invalid) {
        this.$v.$touch()
        return
      }

      this.user.contacts.forEach(contact => {
        if(!contact.isOld) {
          this.editUser.contacts.push({
            contact_name: contact.contact_name,
            url: contact.url
          })
        }
      })

      if(this.editUser.resume.skills) {
        this.editUser.resume.skills = this.editUser.resume.skills.join('#')
      }

      await this.editProfile(this.editUser)
      .then(() => {
        this.isEdit = false
        this.editUser = {
          contacts: [],
          resume: {}
        }
      })
      if(this.avatar.image) {
        await this.setAvatar(this.avatar.image)
        .then(async () => {
          await this.getUserAvatar(this.user.avatar)
        })
      }
    }
  }
}
