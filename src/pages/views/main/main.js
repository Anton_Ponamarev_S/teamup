import { mapActions, mapGetters } from 'vuex'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
export default {
  data: () => ({
    isLogin: true,
  }),
  created () {
    this.isAuth()
  },
  methods: {
    ...mapGetters(['getIsAuth', 'getUser']),
    ...mapActions(['checkAuth', 'getUserAvatar']),

    async isAuth() {
      if(localStorage.getItem('token')) {
        await this.checkAuth()
        .then(async () => {
          await this.getUserAvatar(this.getUser().avatar)
        })
      }
    }
  },
  components: {
    Login,
    Register
  }
}
