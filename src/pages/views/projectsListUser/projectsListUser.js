import { mapActions, mapGetters } from 'vuex'

export default {
  data: () => ({
    projects: []
  }),

  created() {
    this.fetchData()
  },
  methods: {
    ...mapActions(['getAllProjectsUser']),
    ...mapGetters(['getAllProjectsUserList']),

    async fetchData() {
      await this.getAllProjectsUser()
        .then(() => {
          this.projects = this.getAllProjectsUserList()
        })
    }
  }

}
