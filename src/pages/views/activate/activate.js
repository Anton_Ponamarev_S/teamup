import { mapActions } from 'vuex'
export default {
  name: 'activate',
  data: () => ({
    message: ''
  }),
  created () {
    this.activate_profile()
  },
  methods: {
    ...mapActions(['activate']),
    async activate_profile () {
      await this.activate(this.$route.params.token)
      .then(() => {
        this.$router.push('/')
      })
      .catch(e => {
        this.message = e.response?.data?.message
      })
    }
  }
}
