import { mapActions, mapGetters } from 'vuex'
import GeneralInfo from './components/GeneralInfo.vue'
import MembersList from './components/MembersList.vue'
import CandidateList from './components/CandidateList.vue'
import ButtonTeamUp from './components/ButtonTeamUp.vue'
import Settings from './components/Settings.vue'

export default {
  name: 'project',
  components: {
    GeneralInfo,
    MembersList,
    CandidateList,
    ButtonTeamUp,
    Settings
  },

  data: () => ({
    tab: ['members', 'candidates', 'settings'],
    currentTab: 'members',

    project: {
      info: {},
      members: [],
      user: {},
      candidates: []
    },
  }),

  created() {
    this.fetchData()
  },

  methods: {
    ...mapActions(['getAllDataProject', 'getAvatars', 'getAllCandidate']),
    ...mapGetters(['getProject', 'getUser', 'getAvatarsUrl', 'getIsAuth']),


    projectDto(project_data) {

      this.project = project_data
      this.project.info.contacts = this.project.info?.contacts?.split('#')

      this.project.members?.forEach((elem, index) => {
        this.$set(this.project.members[index], 'isEditUser', false)
      })
    },

    async loadImage() {
      if(this.project.members?.length) {
        const avatarsMembers = new Array(this.project.members.lenght)
        this.project.members.forEach((member, index) => {
          avatarsMembers[index] = member.user.avatar
        })

        await this.getAvatars(avatarsMembers)
          .then(() => {
            const membersUrl = this.getAvatarsUrl()

            this.project.members.forEach((member, index) => {
              this.$set(member.user, 'imageUrl', membersUrl[index])
            })
          })
      }


      if(this.project.candidates?.length) {
        const avatarsСandidates = new Array(this.project.candidates.lenght)
        this.project.candidates.forEach((candidate, index) => {
          avatarsСandidates[index] = candidate.user.avatar
        })

        await this.getAvatars(avatarsСandidates)
          .then(() => {
            const сandidatesUrl = this.getAvatarsUrl()

            this.project.candidates.forEach((candidate, index) => {
              this.$set(candidate.user, 'imageUrl', сandidatesUrl[index])
            })
          })
      }
    },

    async fetchData() {
      const projectId = this.$route.params.projectId
      await this.getAllDataProject(projectId)
        .then(async () => {
          if(this.getProject().user.isOwner) {
            await this.getAllCandidate(projectId)
          }

          this.projectDto(this.getProject())
          await this.loadImage()
        })
    }
  }
}
