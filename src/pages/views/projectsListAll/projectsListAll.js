import { mapActions, mapGetters } from 'vuex'

export default {
  data: () => ({
    projects: []
  }),

  created() {
    this.fetchData()
  },
  methods: {
    ...mapActions(['getAllProjects']),
    ...mapGetters(['getAllProjectsList', 'getUser']),

    async fetchData() {
      await this.getAllProjects()
        .then(() => {
          this.projects = this.getAllProjectsList()
        })
    }
  }

}
