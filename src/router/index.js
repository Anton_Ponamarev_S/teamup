import Vue from 'vue'
import VueRouter from 'vue-router'
import { Routes } from '@/helpers/routerConsts'

Vue.use(VueRouter)

const routes = [
  {
    path: Routes.MAIN_PATH,
    name: Routes.MAIN_NAME,
    meta: { layout: 'main' },
    component: () => import('@/pages/views/main/Main.vue')
  },

  {
    path: Routes.ACTIVATE_PATH,
    name: Routes.ACTIVATE_NAME,
    meta: { layout: 'empty' },
    component: () => import('@/pages/views/activate/Activate.vue')
  },

  {
    path: Routes.USERS_LIST_ALL_PATH,
    name: Routes.USERS_LIST_ALL_NAME,
    meta: { layout: 'sidebar' },
    component: () => import('@/pages/views/users/UsersAll.vue')
  },
  {
    path: Routes.PROFILE_MY_PATH,
    name: Routes.PROFILE_MY_NAME,
    meta: { layout: 'sidebar' },
    component: () => import('@/pages/views/profile/ProfileMy.vue')
  },
  {
    path: Routes.PROFILE_USER_PATH,
    name: Routes.PROFILE_USER_NAME,
    meta: { layout: 'sidebar' },
    component: () => import('@/pages/views/profile/ProfileUser.vue')
  },

  {
    path: Routes.PROJECT_CREATE_PATH,
    name: Routes.PROJECT_CREATE_NAME,
    meta: { layout: 'sidebar' },
    component: () => import('@/pages/views/project/ProjectCreate.vue')
  },
  {
    path: Routes.PROJECTS_LIST_ALL_PATH,
    name: Routes.PROJECTS_LIST_ALL_NAME,
    meta: { layout: 'sidebar' },
    component: () => import('@/pages/views/projectsListAll/ProjectsListAll.vue')
  },
  {
    path: Routes.PROJECTS_LIST_MY_PATH,
    name: Routes.PROJECTS_LIST_MY_NAME,
    meta: { layout: 'sidebar' },
    component: () => import('@/pages/views/projectsListUser/ProjectsListUser.vue')
  },
  {
    path: Routes.PROJECT_PATH,
    name: Routes.PROJECT_NAME,
    meta: { layout: 'sidebar' },
    component: () => import('@/pages/views/project/Project.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
