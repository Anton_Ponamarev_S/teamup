import { $api } from "@/helpers/http"

const state = () => ({
  projects: []
})

const mutations = {
  setProjects(state, projects) {
    state.projects = projects
  }
}

const actions = {
  async getAllProjects({ commit }) {
    await $api.get('/project')
      .then(response => {
        commit('setProjects', response.data)
      })
      .catch(e => { throw e })
  },
}

const getters = {
  getAllProjectsList: state => state.projects
}

export default {
  state,
  mutations,
  actions,
  getters
}
