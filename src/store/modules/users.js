import { $api } from "@/helpers/http"

const state = () => ({
  users: []
})

const mutations = {
  setAvatarUrl(state, users) {
    state.users = users
  }
}

const actions = {
  async getUsersList({ commit }) {
    await $api.get('/users')
    .then(response => {
      commit('setAvatarUrl', response.data)
    })
    .catch(e => { throw e })
  },
}

const getters = {
  getUsersAll: state => state.users
}

export default {
  state,
  mutations,
  actions,
  getters
}
