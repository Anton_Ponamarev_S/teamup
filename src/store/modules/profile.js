import { $api } from "@/helpers/http"

const state = () => ({
  profile: {},
  profileUser: {}
})

const mutations = {
  setProfile(state, profile) {
    state.profile = profile
  },
  setProfileUser(state, profileUser) {
    state.profileUser = profileUser
  },
}

const actions = {
  async getUserProfile({ commit }) {
    await $api.get('/profile')
    .then(response => {
      commit('setProfile', response.data)
    })
    .catch(e => { throw e })
  },

  async getUserProfileUser({ commit }, userId) {
    await $api.get(`/profile/${userId}`)
    .then(response => {
      commit('setProfileUser', response.data)
    })
    .catch(e => { throw e })
  },

  async editProfile({ commit }, profileRequest) {
    await $api.put('/profile', profileRequest)
    .catch(e => { throw e })
  },

  async teamUpInProject({ commit }, userData) {
    await $api.post(`/project/${userData.projectId}/candidate/${userData.userId}`, userData.data)
    .catch(e => { throw e })
  },

}

const getters = {
  getProfile: state => state.profile,
  getProfileUser: state => state.profileUser,
}

export default {
  state,
  mutations,
  actions,
  getters
}
