import { $api, $link } from "@/helpers/http"
import { readFileAsDataURL } from '@/helpers/image'

const state = () => ({
  avatarUserUrl: './../img/initial_user_avatar.jpg',

  avatarsUrl: []
})

const mutations = {
  setUserAvatarUrl(state, avatarUserUrl) {
    state.avatarUserUrl = avatarUserUrl
  },

  setAvatarsUrl(state, avatarsUrl) {
    state.avatarsUrl = avatarsUrl
  },

  setAvatarInArray(state, avatarData) {
    state.avatarsUrl[avatarData.index] = avatarData.avatar
  },

  setSizeAvatars(state, size) {
    state.avatarsUrl = new Array(size).fill('/../img/initial_user_avatar.jpg')
  }
}

const actions = {
  async setAvatar({ }, avatarRequest) {
    let formData = new FormData()
    formData.append('avatar', avatarRequest)

    await $api.post('/profile/avatar', formData)
      .catch(e => { throw e })
  },

  async getUserAvatar({ commit }, link) {
    await $link.get(link, { responseType: "blob" })
      .then(response => {
        const fileReaderInstance = new FileReader();
        fileReaderInstance.readAsDataURL(response.data);
        fileReaderInstance.onload = () => {
          commit('setUserAvatarUrl', fileReaderInstance.result)
        }
      })
      .catch(e => { throw e })
  },

  async getAvatars({ commit, dispatch }, Arraylink) {
    commit('setSizeAvatars', Arraylink.length)
    let index = 0
    for (const link of Arraylink) {
      await $link.get(link, { responseType: "blob" })
      .then(async response => {
        let dataURL = await readFileAsDataURL(response.data)
        commit('setAvatarInArray', { avatar: dataURL, index: index })
      })
      .catch(e => {})
      index++
    }
  },
}

const getters = {
  getAvatarUserUrl: state => state.avatarUserUrl,
  getAvatarsUrl: state => state.avatarsUrl,
}

export default {
  state,
  mutations,
  actions,
  getters
}
