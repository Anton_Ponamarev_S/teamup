import { $api } from "@/helpers/http"

const state = () => ({
  projectInfo: {},
  projectMembers: [],
  projectCandidates: [],
  projectUser: {},
  projectId: null
})

const mutations = {
  setProjectInfo(state, projectInfo) {
    state.projectInfo = projectInfo
  },

  setProjectMembers(state, projectMembers) {
    state.projectMembers = projectMembers
  },

  setProjectCandidates(state, projectCandidates) {
    state.projectCandidates = projectCandidates
  },

  setProjectUser(state, projectUser) {
    state.projectUser = projectUser
  },

  setProjectId(state, projectId) {
    state.projectId = projectId
  }
}

const actions = {
  async createProject({ commit }, projectRequest) {
    await $api.post('/project', projectRequest)
    .then(response => {
      commit('setProjectId', response.data.id)
    })
    .catch(e => { throw e })
  },

  async getAllDataProject({ commit }, projectId) {
    await $api.get(`/project/${projectId}`)
    .then(response => {
      commit('setProjectInfo', response.data.info)
      commit('setProjectMembers', response.data.members)
      commit('setProjectUser', response.data.user)
    })
    .catch(e => { throw e })

  },

  async editProjectData({ commit }, projectRequest) {
    await $api.put(`/project/${projectRequest.id}`, projectRequest.data)
    .then(response => {
      commit('setProjectInfo', response.data)
    })
    .catch(e => { throw e })
  },

  async deliteProject({ commit }, projectId) {
    await $api.delete(`/project/${projectId}`)
    .then(() => {
      commit('setProjectInfo', null)
      commit('setProjectMembers', null)
      commit('setProjectUser', null)
    })
    .catch(e => { throw e })
  },

  async getAllCandidate({ commit }, projectId) {
    await $api.get(`/project/${projectId}/candidate`)
    .then(response => {
      commit('setProjectCandidates', response.data)
    })
    .catch(e => { throw e })
  },

  async changeRole({ commit }, userRequest) {
    await $api.put(`/project/${userRequest.projectId}/member/${userRequest.userId}`, userRequest.data)
    // .then(response => {
    // })
    .catch(e => { throw e })
  },

  async removeInTeam({ commit }, userRequest) {
    await $api.delete(`/project/${userRequest.projectId}/member/${userRequest.userId}`)
    .then(() => {
      // 
    })
    .catch(e => { throw e })
  },

  async takeUserInTeam({ commit }, userRequest) {
    await $api.post(`/project/${userRequest.projectId}/candidate/${userRequest.userId}`, userRequest.data)
    .catch(e => { throw e })
  },

  async notTakeUserInTeam({ commit }, userRequest) {
    await $api.delete(`/project/${userRequest.projectId}/candidate/${userRequest.userId}`)
    .catch(e => { throw e })
  },

  async userTeamUp({ commit }, userRequest) {
    await $api.post(`/project/${userRequest.projectId}/candidate/`, userRequest.data)
    .catch(e => { throw e })
  }
}

const getters = {
  getProject: state => ({
    info: state.projectInfo,
    members: state.projectMembers,
    candidates: state.projectCandidates,
    user: state.projectUser,
  }),
  getProjectId: state => state.projectId
}

export default {
  state,
  mutations,
  actions,
  getters
}
