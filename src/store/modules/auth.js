import { $api } from "@/helpers/http"

const state = () => ({
  user: {},
  isAuth: false,
})

const mutations = {
  setUser(state, user) {
    state.user = user
  },

  setIsAuth(state, isAuth) {
    state.isAuth = isAuth;
  },
}

const actions = {
  async login({ commit }, loginRequest) {
    await $api.post('auth/login', loginRequest)
    .then(response => {
      localStorage.setItem('token', response.data.accessToken);
      commit('setIsAuth', true)
      commit('setUser', response.data.user)
    })
    .catch(e => { throw e })
  },

  async register({}, RegistorRequest) {
    await $api.post('auth/register', RegistorRequest)
    .catch(e => { throw e })
  },

  async logout({ commit }) {
    await $api.post('auth/logout')
    .then(() => {
      localStorage.removeItem('token');
      commit('setIsAuth', false)
      commit('setUser', {})
    })
    .catch(e => { throw e })
  },

  async checkAuth({ commit }) {
    await $api.get(`auth/refresh`)
    .then(response => {
      localStorage.setItem('token', response.data.accessToken);
      commit('setIsAuth', true)
      commit('setUser', response.data.user)
    })
    .catch(e => { throw e })
  },

  async activate({ commit }, token) {
    await $api.get(`auth/activate/${token}`)
    .then(response => {
      localStorage.setItem('token', response.data.accessToken);
      commit('setIsAuth', true)
      commit('setUser', response.data.user)
    })
    .catch(e => { throw e })
  }
}

const getters = {
  getUser: state => state.user,
  getIsAuth: state => state.isAuth
}

export default {
  state,
  mutations,
  actions,
  getters
}
