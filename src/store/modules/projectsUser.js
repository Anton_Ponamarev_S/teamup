import { $api } from "@/helpers/http"

const state = () => ({
  projects: []
})

const mutations = {
  setProjects(state, projects) {
    state.projects = projects
  }
}

const actions = {
  async getAllProjectsUser({ commit }) {
    await $api.get('/project/my')
      .then(response => {
        commit('setProjects', response.data)
      })
      .catch(e => { throw e })
  },
}

const getters = {
  getAllProjectsUserList: state => state.projects
}

export default {
  state,
  mutations,
  actions,
  getters
}
