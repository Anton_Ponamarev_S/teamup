import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import auth from './modules/auth'
import profile from './modules/profile'
import avatar from './modules/avatar'
import project from './modules/project'
import projectsListUser from './modules/projectsUser'
import projectsListAll from './modules/projectsAll'
import users from './modules/users'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    profile,
    avatar,
    project,
    projectsListUser,
    projectsListAll,
    users,
  },
  plugins: [createPersistedState({
    storage : window.sessionStorage,
    paths : ['auth.user', 'auth.isAuth', 'avatar.avatarUserUrl']
  })],
})
