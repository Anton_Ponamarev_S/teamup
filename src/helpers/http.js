import axios from 'axios'
import { mapMutations } from 'vuex'

const { setIsAuth, setUser } =  mapMutations(['setIsAuth', 'setUser'])

export const API_URL = 'http://localhost:8081/api'
export const LINK_URL = 'http://localhost:8081'

export const $api = axios.create({
  withCredentials: true,
  baseURL: API_URL,
})

$api.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`
  return config
})

$api.interceptors.response.use((config) => {
  return config;
}, async (error) => {
  const originalRequest = error.config;
  if (error.response.status == 401 && error.config && !error.config._isRetry) {
    originalRequest._isRetry = true;
    await axios.get(`${API_URL}/auth/refresh`, { withCredentials: true })
      .then(response => {
        localStorage.setItem('token', response.data.accessToken);
        setUser(response.data.user)
        setIsAuth(true)
        return $api.request(originalRequest);
      })
      .catch(e => {
        console.log(e)
      })
  }
  throw error;
})

export const $link = axios.create({
  withCredentials: true,
  baseURL: LINK_URL,
})

$link.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`
  return config
})

$link.interceptors.response.use((config) => {
  return config;
}, async (error) => {
  const originalRequest = error.config;
  if (error.response.status == 401 && error.config && !error.config._isRetry) {
    originalRequest._isRetry = true;
    await axios.get(`${API_URL}/auth/refresh`, { withCredentials: true })
      .then(response => {
        localStorage.setItem('token', response.data.accessToken);
        setUser(response.data.user)
        setIsAuth(true)
        return $link.request(originalRequest);
      })
      .catch(e => {
        console.log(e)
      })
  }
  throw error;
})
