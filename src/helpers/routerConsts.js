export const Routes = {
  MAIN_PATH: '/',
  MAIN_NAME: 'main',

  ACTIVATE_PATH: '/activate/:token',
  ACTIVATE_PATH_BY_TOKEN: token => `/activate/${token}`,
  ACTIVATE_NAME: 'activate',

  USERS_LIST_ALL_PATH: '/users',
  USERS_LIST_ALL_NAME: 'usersAll',

  PROFILE_MY_PATH: '/users/profile',
  PROFILE_MY_NAME: 'profileMy',

  PROFILE_USER_PATH: '/users/profile/:userId',
  PROFILE_USER_PATH_BY_ID: userId => `/users/profile/${userId}`,
  PROFILE_USER_NAME: 'profileUser',

  PROJECT_CREATE_PATH: '/projects/create-project',
  PROJECT_CREATE_NAME: 'ProjectCreate',

  PROJECTS_LIST_ALL_PATH: '/projects',
  PROJECTS_LIST_ALL_NAME: 'projectsListAll',

  PROJECTS_LIST_MY_PATH: '/projects/user',
  PROJECTS_LIST_MY_NAME: 'projectsListUser',

  PROJECT_PATH: '/projects/:projectId',
  PROJECT_PATH_BY_ID: projectId => `/projects/${projectId}`,
  PROJECT_NAME: 'project',
};
